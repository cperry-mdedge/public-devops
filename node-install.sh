#!/bin/bash
# Install NVM
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
# Load NVM
wait
. ~/.nvm/nvm.sh
# Install node
wait
nvm install node
