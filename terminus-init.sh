#!/bin/bash
# Create Terminus dir
mkdir /home/ubuntu/terminus
cd /home/ubuntu/terminus
# Download and run Terminus installer
curl -O https://raw.githubusercontent.com/pantheon-systems/terminus-installer/master/builds/installer.phar && sudo php installer.phar install
# Authenticate
terminus auth:login --machine-token=$1
# Generate aliases
terminus aliases
